require('dotenv').config();
const express = require("express");
const app = express();
const router = require('./router.js');
const cors = require('cors');
const morgan = require('morgan');

app.use(cors());

app.use(express.json());

if (process.env.NODE_ENV !== 'test')
    app.use(morgan('dev'))

app.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: 'Hello World'
    })
})

app.use(express.urlencoded({ extended: true }))

app.use('/api/v1', router);

module.exports = app;
