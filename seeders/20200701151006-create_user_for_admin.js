'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.bulkInsert('users', [{
        role: 'admin',
        email: 'admin1@mail.com',
        password: bcrypt.hashSync('12345678', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date()
      }]),
      queryInterface.bulkInsert('profiles', [{
        first_name: 'admin',
        last_name: 'ganteng',
        bank: 'Bank ABC',
        location: 'Indonesia, Jakarta',
        acc_number: 123456789,
        picture: 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png',
        user_id: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      }])
      
    ], {});

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
