const { user, profile } = require('../models')
const response = require('../helpers/response')
const Validator = require('fastest-validator')
const val = new Validator()
const bcrypt = require('bcryptjs')
const token = require('../helpers/token');


module.exports = {
    async register(req, res) {
        let { 
            first_name, 
            last_name,
            email,
            password,
            verify_password,
            role
        } = req.body

        try {
            // Validation schema
            const allowedInput = {
                first_name: {type: 'string', required: true},
                last_name: {type: 'string', required: true},
                email: {type: 'email', empty: false},
                password: {type: 'string', required: true, length: 8, alphanum: true},
                role: {type: "enum", values: ["participant", "provider", "admin"], empty: false}
            }
            
            const isValid = val.validate(req.body, allowedInput)
            if(Array.isArray(isValid))
                return res.status(400).json({
                    status: 'fail',
                    errors: isValid
                })
            
            // Check if the name is available
            const checkName = await profile.findOne({
                where: {
                    first_name,
                    last_name
                }
            })
            if (checkName) throw new Error ('Your name is already taken')

            // Check if email is already registered
            const checkEmail = await user.findOne({
                where: {
                email
                }
            })
            
            if (checkEmail) throw new Error ('Email is already registered')

            
            // Password match verification
            if (password !== verify_password) throw new Error ("Sorry password doesn't match. Please try again")
            
            // User creation 
            await user.create({
                email,
                password,
                role,
            }) 

            // Profile creation
            await profile.create({
                first_name,
                last_name,
                user_id: user.id
            })


            res.status(201).json(response.success('Successfuly registered!', {user, profile}))
        }
        catch (err) {
            return res.status(401).json(response.error('Register Failed!', err.message))
        }
    },

    async login(req, res) {
        try {
            let instance = await user.findOne({
                where: {
                    email: req.body.email.toLowerCase()
                }
            })
            if(!instance) {
                throw new Error(`email ${req.body.email} doesn't exist!`)
            }
            const isPasswordTrue = await bcrypt.compareSync(req.body.password, instance.password)
            if(!isPasswordTrue) {
                throw new Error(`Wrong Password!`)
            }
            return res.status(201).json(response.success('Successfuly login!', { token: token(instance)}))
        } catch (err) {
            errorResponse(res,403, [err.message])
        }
    },

    async profile(req,res){
        const data = await user.findByPk(req.user.id, {
            attributes: ['id', 'email', 'role'],
            include: [{
                model: profile,
                attributes: ['id', 'first_name', 'last_name']
            }]
        })
        successResponse(res, 200, data)
    }
    
    
    
}

