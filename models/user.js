'use strict';
const bcrypt = require('bcryptjs');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user.hasOne(models.profile, {
        foreignKey: 'user_id'
      }),
      user.hasOne(models.submission, {
        foreignKey: 'user_id'
      }),
      user.hasMany(models.contest, {
        foreignKey: 'user_id'
      })
    }
  };
  user.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          msg: "It must be an email!"
        }
      },
      unique: {
        args: true,
        msg: 'Email already registered'
      }
    },
    password: {
    type: DataTypes.STRING,
      validate: {
        len: {
          args: [8],
          msg: 'Password should be at least 8 characters'
        }
      }
    },
    role: DataTypes.STRING,
   
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
    },
      beforeCreate: instance => {
      instance.password = bcrypt.hashSync(instance.password, 10)
      // instance.email = instance.email.toLowerCase();
    }
  },
    sequelize,
    modelName: 'user',
  });
  return user;
};
