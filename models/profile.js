'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class profile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      profile.belongsTo(models.user, {
        foreignKey: 'user_id'
      })
      // define association here
    }
  };
  profile.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    bank: {
      type: DataTypes.STRING,
      defaultValue: 'Bank ABC'
    },
    acc_number: {
      type: DataTypes.INTEGER,
      defaultValue: 123456
    },
    picture: {
      type: DataTypes.TEXT,
      defaultValue: 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png'
    },
    location: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'profile',
  });
  return profile;
};
