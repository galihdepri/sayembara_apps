'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class submission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      submission.belongsTo(models.user, {
        foreignKey: 'user_id'
      }),
      submission.belongsTo(models.contest, {
        foreignKey: 'contest_id'
      })

    }
  };
  submission.init({
    description: DataTypes.TEXT,
    file: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      validate: {
        min: 1
      }
    },
    user_id: DataTypes.INTEGER,
    contest_id: DataTypes.INTEGER,
    sub_status: DataTypes.BOOLEAN,
  }, {
    sequelize,
    modelName: 'submission',
  });
  return submission;
};
