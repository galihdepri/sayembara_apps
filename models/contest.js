'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class contest extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      contest.belongsTo(models.payment, {
        foreignKey: 'payment_id'
      }),
      contest.hasMany(models.submission, {
        foreignKey: 'contest_id'
      }),
      contest.belongsTo(models.user, {
        foreignKey: 'user_id'
      })
    }
  };
  contest.init({
    prize: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: {
          arg: true,
          msg: 'Prize only allowed number'
        }
      }
    },
    due_date: DataTypes.DATE,
    announcement: DataTypes.DATE,
    description: {
      type: DataTypes.TEXT,
      validate: {
        is: {
          arg: /^[a-z]+$/i,
          msg: 'Description only allow letters'
        }
      }
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'pending'
    },
    payment_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'contest',
  });
  return contest;
};

