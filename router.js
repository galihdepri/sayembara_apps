const router = require('express').Router()
const userController = require('./controllers/userController')
const authenticate = require('./middlewares/authenticate')

router.post('/user/register/', userController.register )
router.post('/user/login/', userController.login )
router.get('/user/profile/', authenticate, userController.profile )


module.exports = router;